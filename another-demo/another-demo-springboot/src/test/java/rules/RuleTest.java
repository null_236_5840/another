package rules;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.focus.another.core.rules.*;
import org.focus.another.core.rules.match.MatchManager;
import org.focus.another.extension.datasource.Converter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuleTest {

    public static final String rulePath = "/another/appName/betaresource";

    @Test
    public void test(){
        Map<String, Rule> resourceRules = new HashMap<String, Rule>();
        String resourceRulesStr = initRule(resourceRules);
        System.out.println(resourceRulesStr);


        Converter<String, Map<String, String>> converter = new Converter<String, Map<String, String>>() {
            public Map<String, String> convert(String source) {
                final Map stringRuleMap = JSON.parseObject(source, new TypeReference<Map<String,String>>(){});
                return stringRuleMap;
            }
        };
        Map<String,String> map = converter.convert(resourceRulesStr);
        for (String key : map.keySet()){
            System.out.println(key);
            Rule rule = (Rule) JSON.parseObject(map.get(key),RuleManager.getClassByKey(key));
            System.out.println( rule.check());
        }


    }


    private String initRule(Map<String, Rule> resourceRules){
        SwitchRule switchRule = new SwitchRule();
        switchRule.setOpenOrNot(true);

        HostRule hostRule = new HostRule();
        hostRule.setIps("192.168.0.1,192.168.0.2");


        MatchRule matchRule = new MatchRule();
        List<MatchRule.Condition> conditions = new ArrayList<MatchRule.Condition>();
        MatchRule.Condition condition = new MatchRule.Condition();
        condition.setParameterAnnotaionName("orderMain");
        condition.setValue("1105368");
        condition.setMatchStrategy(MatchManager.CONTAIN_STRATEGY);
        condition.setFieldName("orgCode");
        conditions.add(condition);

        MatchRule.Condition conditionStation = new MatchRule.Condition();
        conditionStation.setParameterAnnotaionName("orderMain");
        conditionStation.setValue(124);
        conditionStation.setMatchStrategy(MatchManager.LARGERTHAN_STRATEGY);
        conditionStation.setFieldName("stationId");
        conditions.add(conditionStation);

        MatchRule.Condition conditionOrderId = new MatchRule.Condition();
        conditionOrderId.setParameterAnnotaionName("orderMain");
        conditionOrderId.setValue(122L);
        conditionOrderId.setMatchStrategy(MatchManager.LESSTHAN_STRATEGY);
        conditionOrderId.setFieldName("orderId");
        conditions.add(conditionOrderId);
        Map<String,List<MatchRule.Condition>> listMap = new HashMap<>();
        listMap.put("orderMain",conditions);
        matchRule.setMapConditions(listMap);




        FrequencyRule frequencyRule = new FrequencyRule();
        frequencyRule.setInitLimit(1);
        frequencyRule.setLimitStep(3);
        frequencyRule.setFrequencyMode(true);
        frequencyRule.setTimeWindowSec((5 * 60));
        frequencyRule.setStepCount(1);
        frequencyRule.setLastSetTime(System.currentTimeMillis());

        resourceRules.put("switchRule",switchRule);
        resourceRules.put("matchRule",matchRule);
        resourceRules.put("hostRule",hostRule);
        resourceRules.put("frequencyRule",frequencyRule);

        System.out.println(JSON.toJSONString(resourceRules));
        return JSON.toJSONString(resourceRules);
    }




}
