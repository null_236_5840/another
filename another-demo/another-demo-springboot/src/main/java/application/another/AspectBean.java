package application.another;

import org.focus.another.extension.aspectj.AnotherResourceAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AspectBean {
    @Bean
    public AnotherResourceAspect anotherResourceAspect(){
        return new AnotherResourceAspect();
    }
}
