package application.another;

import org.focus.another.extension.datasource.zookeeper.ZookeeperReadDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

    @Configuration
    public class AnotherDatasourceIniter {
        //- zookeeper主机地址
        @Value("${another.zookeeper.host}")
        private String host;
        //- 应用唯一名称
        @Value("${another.appname}")
        private String appName;
        //- 离线存储地址，如为空，则用默认地址
        @Value("${another.offlinepath}")
        private String offlinePath;

        @Bean
        public ZookeeperReadDataSource zookeeperDataSource(){
            return new ZookeeperReadDataSource(appName,host,offlinePath);
        }
    }