package application.biz;

import org.focus.another.core.annotation.AnotherParameterMatch;
import org.focus.another.core.annotation.AnotherResource;
import org.springframework.stereotype.Service;

@Service
public class TestService {


    @AnotherResource(name="submitorder",anotherMethod="queryOrderAnother")
    public void queryOrder(@AnotherParameterMatch("username") String username,
                           @AnotherParameterMatch("mobile") Long mobile,
                           @AnotherParameterMatch("orderMain") OrderMain orderMain){
        System.out.println("执行老方法....");
    }


    public void queryOrderAnother(String username,Long mobile,OrderMain orderMain){
        System.out.println("通过another执行新方法....");
    }

}
