package application;

import application.biz.OrderMain;
import application.biz.TestService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application {


    public static void main(String[] args) throws InterruptedException {
        ApplicationContext applicationContext = SpringApplication.run(Application.class,args);
        TestService testService = (TestService) applicationContext.getBean("testService");

        OrderMain orderMain = new OrderMain();
        orderMain.setOrderId(123L);
        orderMain.setOrgCode("1105368");
        orderMain.setStationId(123);

        while(true){
            testService.queryOrder("nero",8956425L,orderMain);
            Thread.sleep(2000L);
        }
    }



}
