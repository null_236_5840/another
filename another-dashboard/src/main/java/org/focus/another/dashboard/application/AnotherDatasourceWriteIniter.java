package org.focus.another.dashboard.application;

import org.focus.another.extension.datasource.zookeeper.ZookeeperWriteDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AnotherDatasourceWriteIniter {
    @Value("${another.zookeeper.host}")
    private String host;
    private String rootPath="/another";

    @Bean
    public ZookeeperWriteDataSource zookeeperDataSource(){
        return new ZookeeperWriteDataSource(rootPath,host);
    }
}