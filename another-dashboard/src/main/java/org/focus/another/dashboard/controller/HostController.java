package org.focus.another.dashboard.controller;

import org.apache.commons.lang.StringUtils;
import org.focus.another.dashboard.common.Response;
import org.focus.another.extension.datasource.DataSourceWriteOperator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("host")
public class HostController {

    @Resource
    DataSourceWriteOperator dataSourceWriteOperator;

    @RequestMapping("load")
    @ResponseBody
    public Response loadHostByApp(@RequestParam String appName){
        if (StringUtils.isBlank(appName)){
            return new Response(-1,"appName can't be null.");
        }

        List<String> list = dataSourceWriteOperator.loadHosts(appName);
        return new Response<List<String>>(200,"op success",list);
    }




}
