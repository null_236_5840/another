package org.focus.another.dashboard.controller;

import org.apache.commons.lang.StringUtils;
import org.focus.another.core.rules.RuleValidater;
import org.focus.another.dashboard.bean.ResourceRequest;
import org.focus.another.dashboard.common.Response;
import org.focus.another.extension.datasource.DataSourceWriteOperator;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("resource")
public class ResourceController {

    @Resource
    DataSourceWriteOperator dataSourceWriteOperator;



    @RequestMapping("load")
    @ResponseBody
    public Response loadResourcesByApp(@RequestParam String appName){
        if (StringUtils.isBlank(appName)){
            return new Response(-1,"appName can't be null.");
        }

        List<String> list = dataSourceWriteOperator.loadResourcesByAppName(appName);
        return new Response<List<String>>(200,"op success",list);
    }


    @RequestMapping("delete")
    @ResponseBody
    public Response deleteResource(@RequestParam String appName,@RequestParam String resource){
        if (StringUtils.isBlank(appName)){
            return new Response(-1,"appName can't be null.");
        }
        if (StringUtils.isBlank(resource)){
            return new Response(-1,"resource can't be null.");
        }

        dataSourceWriteOperator.deleteResource(appName,resource);
        return new Response<List<String>>(200,"op success");
    }



    @RequestMapping("update")
    @ResponseBody
    public Response updateResource(@RequestBody ResourceRequest resourceRequest){
        String appName = resourceRequest.getAppName();
        String resource = resourceRequest.getResourceName();
        String rules = resourceRequest.getRules();

        if (StringUtils.isBlank(appName)){
            return new Response(-1,"appName can't be null.");
        }
        if (StringUtils.isBlank(resource)){
            return new Response(-1,"resource can't be null.");
        }
        if (StringUtils.isBlank(rules)){
            return new Response(-1,"rules can't be null.");
        }
        if (!RuleValidater.validate(rules)){
            return new Response(-1,"格式验证不通过，请查阅帮助填写!");
        }



        dataSourceWriteOperator.updateResource(appName,resource,rules);
        return new Response<List<String>>(200,"op success");
    }


    @RequestMapping("add")
    @ResponseBody
    public Response addResource(@RequestBody ResourceRequest resourceRequest){
        String appName = resourceRequest.getAppName();
        String resource = resourceRequest.getResourceName();
        String rules = resourceRequest.getRules();


        if (StringUtils.isBlank(appName)){
            return new Response(-1,"appName can't be null.");
        }
        if (StringUtils.isBlank(resource)){
            return new Response(-1,"resource can't be null.");
        }
        if (StringUtils.isBlank(rules)){
            return new Response(-1,"rules can't be null.");
        }
        if (!RuleValidater.validate(rules)){
            return new Response(-1,"格式验证不通过，请查阅帮助填写!");
        }

        dataSourceWriteOperator.addResource(appName,resource,rules);
        return new Response<List<String>>(200,"op success");
    }


    @RequestMapping("view")
    @ResponseBody
    public Response viewResource(@RequestBody ResourceRequest resourceRequest){
        String appName = resourceRequest.getAppName();
        String resource = resourceRequest.getResourceName();


        if (StringUtils.isBlank(appName)){
            return new Response(-1,"appName can't be null.");
        }
        if (StringUtils.isBlank(resource)){
            return new Response(-1,"resource can't be null.");
        }

        String resourceRules = dataSourceWriteOperator.loadRulesByResourceName(appName,resource);
        return new Response<String>(200,"op success",resourceRules);
    }





}
