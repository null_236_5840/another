package org.focus.another.dashboard.controller;


import org.focus.another.dashboard.common.Response;
import org.focus.another.extension.datasource.DataSourceWriteOperator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("app")
public class AppController {

    @Resource
    DataSourceWriteOperator dataSourceWriteOperator;



    @RequestMapping("load")
    @ResponseBody
    public Response load(){
        List<String> listApp = dataSourceWriteOperator.loadAllApps();
        return new Response(200,"op successs",listApp);
    }
}
