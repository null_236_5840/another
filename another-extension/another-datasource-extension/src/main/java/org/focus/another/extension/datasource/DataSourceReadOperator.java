package org.focus.another.extension.datasource;

import org.focus.another.core.rules.Rule;

import java.util.Map;

public interface DataSourceReadOperator {
    /****
     * @Description  根据资源名，获取对应的资源
     * @Author zhanglei
     * @Date 2021/3/30 5:53 下午
     * @param sourceName :
     * @return : java.util.Map<java.lang.String,org.focus.another.core.rules.Rule>
     **/
    Map<String, Rule> getRulesByAnotherSource(String sourceName);
    /****
     * @Description 关闭数据源
     * @Author zhanglei
     * @Date 2021/3/10 5:05 下午
     **/
    void close();
}
