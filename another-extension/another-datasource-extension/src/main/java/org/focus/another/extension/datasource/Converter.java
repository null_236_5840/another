package org.focus.another.extension.datasource;


/***
 * @Description 转换器抽象类，将source 转换为target类型
 * @Author zhanglei
 * @Date 2021/3/5 6:01 下午
 **/
public interface Converter<S,T> {

    T convert(S source);
}
