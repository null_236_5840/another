package org.focus.another.extension.datasource;

import org.focus.another.core.rules.Rule;

import java.util.List;

public interface DataSourceWriteOperator {
    /****
     * @Description  获取目前配置的所有app
     * @Author zhanglei
     * @Date 2021/3/31 10:54 上午
     * @return : java.util.List<java.lang.String>
     **/
    List<String> loadAllApps();

    /****
     * @Description 获取app下的所有资源
     * @Author zhanglei
     * @Date 2021/3/31 10:54 上午
     * @return : java.util.List<java.lang.String>
     **/
    List<String> loadResourcesByAppName(String appName);

    /****
     * @Description 获取资源对应的规则
     * @Author zhanglei
     * @Date 2021/3/31 10:55 上午
     * @return : java.util.List<org.focus.another.core.rules.Rule>
     **/
    String loadRulesByResourceName(String appName,String resourceName);

    /****
     * @Description 更新资源对应的规则
     * @Author zhanglei
     * @Date 2021/3/31 10:59 上午
     * @param appName :
     * @param resourceName :
     * @param rules :
     * @return : void
     **/
    void updateResource(String appName,String resourceName,String rules);

    /****
     * @Description 删除资源
     * @Author zhanglei
     * @Date 2021/3/31 10:59 上午
     * @return : void
     **/
    void deleteResource(String appName,String resourceName);

    /****
     * @Description 配置一个资源
     * @Author zhanglei
     * @Date 2021/3/31 10:59 上午
     * @return : void
     **/
    void addResource(String appName,String resourceName,String rules);

    /****
     * @Description 获取应用下所有在线的host
     * @Author zhanglei
     * @Date 2021/4/8 10:09 下午
     **/
    List<String> loadHosts(String appName);
}
