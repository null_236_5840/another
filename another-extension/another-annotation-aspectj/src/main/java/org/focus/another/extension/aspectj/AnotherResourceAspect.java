package org.focus.another.extension.aspectj;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.focus.another.core.annotation.AnotherResource;
import org.focus.another.core.rules.MatchRule;
import org.focus.another.core.rules.Rule;
import org.focus.another.extension.datasource.DataSourceReadOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;

@Aspect
public class AnotherResourceAspect implements ApplicationContextAware {
    private static final Logger logger = LoggerFactory.getLogger(AnotherResourceAspect.class);
    private ApplicationContext applicationContext = null;


    @Pointcut("@annotation(org.focus.another.core.annotation.AnotherResource)")
    public void anotherResourceAspect(){

    }

    @Around(value="anotherResourceAspect()")
    public Object aroundAnotherResource(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        try{
            //- 解析annotation
            Method originMethod = resolveMethod(proceedingJoinPoint);
            AnotherResource anotherResourceAnnotation = originMethod.getAnnotation(AnotherResource.class);
            if (null == anotherResourceAnnotation){
                throw new IllegalStateException("Wrong state for anotherResourceAnnotation annotation");
            }



            DataSourceReadOperator dataSourceOperator = resolveOperator();
            Map<String, Rule> zkRules = dataSourceOperator.getRulesByAnotherSource(anotherResourceAnnotation.name());
            if (null == zkRules){
                return proceedingJoinPoint.proceed();
            }


            boolean invokeNew = true;
            for (Rule rule : zkRules.values()){
                if (rule instanceof MatchRule){
                    //- 解析参数，并且匹配对应的规则。
                    Object[] parameterObjects = proceedingJoinPoint.getArgs();
                    Parameter[] parameters = originMethod.getParameters();
                    invokeNew = rule.check(parameterObjects,parameters);
                }else{
                    invokeNew = rule.check();
                }

                if (!invokeNew){
                    break;
                }
            }

            //- 根据rule 调用对应的方法
            if (invokeNew){
                Method method = resolveMethodByName(proceedingJoinPoint,anotherResourceAnnotation.anotherMethod());
                return method.invoke(proceedingJoinPoint.getTarget(),proceedingJoinPoint.getArgs());
            }else{
                return proceedingJoinPoint.proceed();
            }
        }catch(Throwable e){
            e.printStackTrace();
            logger.error("执行AnotherProxy错误，回滚到原来实现.对应异常:{}",e.getMessage(),e);
            return proceedingJoinPoint.proceed();
        }
    }


    /****
     * @Description
     * @Author zhanglei
     * @Date 2021/3/29 8:38 下午
     **/
    private Method resolveMethod(ProceedingJoinPoint proceedingJoinPoint){
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        Class<?> targetClass = proceedingJoinPoint.getTarget().getClass();

        Method method = getDeclaredMethodFor(targetClass,signature.getName(),
                signature.getMethod().getParameterTypes());
        if (null == method){
            throw new IllegalStateException("Cannot resolve target method: " + signature.getMethod().getName());
        }
        return method;
    }





    /****
     * @Description  查找备用方法.
     * @Author zhanglei
     * @Date 2021/3/29 8:33 下午
     * @param proceedingJoinPoint :
     * @param methodName : 备用方法名
     * @return : java.lang.reflect.Method
     **/
    private Method resolveMethodByName(ProceedingJoinPoint proceedingJoinPoint,String methodName){
        Class<?> targetClass = proceedingJoinPoint.getTarget().getClass();
        Method[] methods = targetClass.getDeclaredMethods();
        for (Method method : methods){
            if (method.getName().equals(methodName)){
                return method;
            }
        }
        throw new IllegalStateException("Cannot resolve another method: " + methodName);
    }





    /****
     * @Description 递归类结构获取对应的method对象
     * @Author zhanglei
     * @Date 2021/3/10 10:16 上午
     * @param clazz : aspect 对应的class对象
     * @param name : 方法签名name
     * @param parameterTypes : 方法的
     * @return : java.lang.reflect.Method
     **/
    private Method getDeclaredMethodFor(Class<?> clazz,String name,Class<?>... parameterTypes){
        try{
            return clazz.getDeclaredMethod(name,parameterTypes);
        } catch (NoSuchMethodException e) {
            Class<?> superClazz = clazz.getSuperclass();
            if (superClazz != null){
                return getDeclaredMethodFor(superClazz,name,parameterTypes);
            }
        }
        return null;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }




    /****
     * @Description 统一获取DataSourceOperator
     * @Author zhanglei
     * @Date 2021/3/30 6:09 下午
     **/
    private DataSourceReadOperator resolveOperator(){
        //- 根据资源名获取对应rule，验证rule
        Map<String, DataSourceReadOperator> dataSourceOperatorMap = applicationContext.getBeansOfType(DataSourceReadOperator.class);
        if (dataSourceOperatorMap.size() > 1){
            throw new IllegalStateException("expected one datasource.but found two");
        }
        if (dataSourceOperatorMap.size() == 0){
            throw new IllegalStateException("you must define one DataSourceOperator bean.");
        }

        DataSourceReadOperator op = null;
        for (DataSourceReadOperator dataSourceOperator : dataSourceOperatorMap.values()){
            op = dataSourceOperator;
        }
        return op;
    }



}
