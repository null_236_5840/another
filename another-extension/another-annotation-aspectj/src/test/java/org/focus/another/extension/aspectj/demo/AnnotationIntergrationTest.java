package org.focus.another.extension.aspectj.demo;

import org.focus.another.core.result.AnotherCallbackResult;
import org.focus.another.extension.aspectj.demo.aop.AopTestConfig;
import org.focus.another.extension.aspectj.demo.service.OrderService;
import org.junit.Test;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {AnnotationIntergrationTest.class,AopTestConfig.class})
public class AnnotationIntergrationTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    OrderService orderService;


    @Test
    public void test(){
        assertThat(AopUtils.isAopProxy(orderService)).isTrue();
        AnotherCallbackResult anotherCallbackResult = new AnotherCallbackResult() {
            public boolean checkResult() {
                //- custom impl for result compare
                return false;
            }
        };
        orderService.queryOrder(System.currentTimeMillis(),anotherCallbackResult);
    }



}
