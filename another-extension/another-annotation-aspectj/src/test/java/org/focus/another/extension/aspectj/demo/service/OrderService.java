package org.focus.another.extension.aspectj.demo.service;

import org.focus.another.core.annotation.AnotherParameterMatch;
import org.focus.another.core.annotation.AnotherResource;
import org.focus.another.core.result.AnotherCallbackResult;
import org.springframework.stereotype.Service;

@Service
public class OrderService {


    @AnotherResource(name="order.queryOrder",anotherMethod="queryOrderAnother")
    public void queryOrder(Long orderId, @AnotherParameterMatch AnotherCallbackResult anotherCallbackResult){
        System.out.println("execute queryOrder....");
    }


    public void queryOrderAnother(Long orderId){
        System.out.println("execute queryOrderAnother....");
    }




}
