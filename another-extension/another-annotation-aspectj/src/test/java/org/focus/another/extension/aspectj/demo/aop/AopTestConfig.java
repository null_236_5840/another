package org.focus.another.extension.aspectj.demo.aop;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan("org.focus")
public class AopTestConfig {

    @Bean
    public String a(){
        return "";
    }



}
