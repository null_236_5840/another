package org.focus.another.extension.datasource.zookeeper;

/***
 * @Description 统一构建目录工具类
 * @Author zhanglei
 * @Date 2021/4/8 10:46 下午
 **/
public class ZookeeperPathBuilder {

    private final static String DIRECTORY_SEPARATE = "/";
    private final static String DIRECTORY_PREFIX = "/another";
    private final static String DIRECTORY_HOSTS = "/onlinehosts";
    private final static String DIRECTORY_RESOURCES = "/resources";


    /***
     * @Description 构建对应的appPath
     * @Author zhanglei
     * @Date 2021/4/9 9:47 上午
     **/
    public static final String buildAppPath(String appName){
        return DIRECTORY_PREFIX+DIRECTORY_SEPARATE+appName;
    }



    public static final String buildResourcePath(String appPath){
        return appPath+DIRECTORY_RESOURCES;
    }


    /****
     * @Description 构建host上报path   /another/appname/hosts/{host}
     * @Author zhanglei
     * @Date 2021/4/9 9:49 上午
     * @param appPath : 应用级别的zkPath
     * @param localHost : 本机ip，用于创建临时节点
     * @return : java.lang.String zk全路径
     **/
    public static final String buildHostUploadPath(String appPath,String localHost){
        return appPath+DIRECTORY_HOSTS+DIRECTORY_SEPARATE+localHost;
    }



    /****
     * @Description 构建hostpath， /another/appname/hosts
     * @Author zhanglei
     * @Date 2021/4/9 3:14 下午
     * @param appPath :
     * @return : java.lang.String
     **/
    public static final String buildHostLoadPath(String appPath){
        return appPath+DIRECTORY_HOSTS;
    }



    /***
     * @Description  获取具体的资源路径    /another/appName/resources/{resourceName}
     * @Author zhanglei
     * @Date 2021/4/9 2:26 下午
     * @param appPath : /another/appname
     * @param resourceName : 具体的资源名
     * @return : java.lang.String
     **/
    public static final String buildDetailRulePath(String appPath,String resourceName){
        return appPath + DIRECTORY_RESOURCES + DIRECTORY_SEPARATE + resourceName;
    }




}
