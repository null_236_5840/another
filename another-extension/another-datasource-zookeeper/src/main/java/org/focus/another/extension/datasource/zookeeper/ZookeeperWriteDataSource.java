package org.focus.another.extension.datasource.zookeeper;

import org.apache.commons.lang.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.focus.another.extension.datasource.DataSourceWriteOperator;

import java.util.ArrayList;
import java.util.List;

public class ZookeeperWriteDataSource implements DataSourceWriteOperator {

    //- 重试间隔1s，最多重试3次
    private static final int RETRY_TIMES = 3;
    private static final int SLEEP_TIME = 1000;
    //- property
    private final String path;
    private final String serverAddr;
    private final CuratorFramework zkClient;


    public ZookeeperWriteDataSource(String path, String serverAddr) {
        if (StringUtils.isBlank(serverAddr) || StringUtils.isBlank(path)) {
            throw new IllegalArgumentException(String.format("Bad argument: serverAddr=[%s], path=[%s]", serverAddr, path));
        }
        this.path = path;
        this.serverAddr = serverAddr;
        this.zkClient = CuratorFrameworkFactory.newClient(serverAddr, new ExponentialBackoffRetry(SLEEP_TIME, RETRY_TIMES));
        zkClient.start();
    }


    @Override
    public List<String> loadAllApps() {
        try{
           List<String> list = zkClient.getChildren().forPath(path);
           return list;
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    @Override
    public List<String> loadResourcesByAppName(String appName) {
        try{
            String appPath = ZookeeperPathBuilder.buildAppPath(appName);
            String resourcePath = ZookeeperPathBuilder.buildResourcePath(appPath);
            if (null == zkClient.checkExists().forPath(resourcePath)){
                return new ArrayList<>();
            }
            List<String> list = zkClient.getChildren().forPath(resourcePath);
            return list;
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    @Override
    public String loadRulesByResourceName(String appName,String resourceName) {
        try{
            String appPath = ZookeeperPathBuilder.buildAppPath(appName);
            String rulePath = ZookeeperPathBuilder.buildDetailRulePath(appPath,resourceName);
            byte[] bytes = zkClient.getData().forPath(rulePath);
            String source = new String(bytes, "utf-8");
            return source;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void updateResource(String appName, String resourceName, String rules) {
        try{
            String appPath = ZookeeperPathBuilder.buildAppPath(appName);
            String rulePath = ZookeeperPathBuilder.buildDetailRulePath(appPath,resourceName);
            zkClient.setData().forPath(rulePath,rules.getBytes());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void deleteResource(String appName, String resourceName) {
        try{
            String appPath = ZookeeperPathBuilder.buildAppPath(appName);
            String rulePath = ZookeeperPathBuilder.buildDetailRulePath(appPath,resourceName);
            zkClient.delete().forPath(rulePath);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void addResource(String appName, String resourceName, String rules) {
        try{
            String appPath = ZookeeperPathBuilder.buildAppPath(appName);
            String rulePath = ZookeeperPathBuilder.buildDetailRulePath(appPath,resourceName);
            if (null == zkClient.checkExists().forPath(rulePath)){
                    zkClient.create().creatingParentsIfNeeded().forPath(rulePath);
            }
            zkClient.setData().forPath(rulePath,rules.getBytes());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<String> loadHosts(String appName) {
        try{
            String appPath = ZookeeperPathBuilder.buildAppPath(appName);
            String hostsPath = ZookeeperPathBuilder.buildHostLoadPath(appPath);
            return zkClient.getChildren().forPath(hostsPath);
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
