package org.focus.another.extension.datasource.zookeeper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.lang.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.Stat;
import org.focus.another.core.concurrent.NamedThreadFactory;
import org.focus.another.core.offline.OfflineOperator;
import org.focus.another.core.rules.Rule;
import org.focus.another.core.rules.RuleManager;
import org.focus.another.core.utils.HostUtil;
import org.focus.another.extension.datasource.AbstractDataSource;
import org.focus.another.extension.datasource.DataSourceReadOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/****
 * @Description TODO-ZL add  log and exceptions,add zookeeper with auth connect
 * @Author zhanglei
 * @Date 2021/3/11 5:09 下午
 **/
public class ZookeeperReadDataSource extends AbstractDataSource implements DataSourceReadOperator {
    private static final Logger logger = LoggerFactory.getLogger(ZookeeperReadDataSource.class);

    //- 重试间隔1s，最多重试3次
    private static final int RETRY_TIMES = 1;
    private static final int SLEEP_TIME = 5000;

    //- zk客户端
    private CuratorFramework zkClient = null;

    //- listener update threadpool
    private final ExecutorService syncPool = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS,
            new ArrayBlockingQueue<Runnable>(10), new NamedThreadFactory("another-zookeeper-datasource-update"),
            new ThreadPoolExecutor.DiscardOldestPolicy());

    private final Runnable syncTask = new Runnable() {
        @Override
        public void run() {
            offlineOperator.writeDisk(JSON.toJSONString(ruleMapCache));
        }
    };

    //- property
    private final String path;
    private final String serverAddr;
    private final String offlinePath;
    private final OfflineOperator offlineOperator;

    public ZookeeperReadDataSource(String appName, String serverAddr,String offlinePath) {
        if (StringUtils.isBlank(serverAddr) || StringUtils.isBlank(appName)) {
            throw new IllegalArgumentException(String.format("Bad argument: serverAddr=[%s], path=[%s]", serverAddr, appName));
        }
        this.path = ZookeeperPathBuilder.buildAppPath(appName);
        this.serverAddr = serverAddr;
        this.offlinePath = offlinePath;
        this.offlineOperator = new OfflineOperator(offlinePath);

        try{
            initZookeeper();
        }catch(KeeperException.ConnectionLossException e){
            logger.error("链接zk初始化失败.尝试读取本地最后一次缓存规则");
            offlineOperator.loadToRuleMap(ruleMapCache);
            logger.error("链接zk初始化失败.load本地规则成功. 等待zk重连替换本地缓存.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /***
     * @Description 将本机注册为zk临时目录，以使得dashboard的host规则可选。
     * @Author zhanglei
     * @Date 2021/4/19 6:11 下午
     **/
    private void regHosts() {
        try{
            String localHost = HostUtil.getLocalHost();
            String hostDir = ZookeeperPathBuilder.buildHostUploadPath(path,localHost);

            if (null == zkClient.checkExists().forPath(hostDir)){
                zkClient.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath(hostDir);
                logger.info("another注册dashboard可选host成功,ip:{}",hostDir);
            }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("初始化Another-上报dashboard host失败,exp:{}",e.getMessage(),e);
        }
    }



    /***
     * @Description  初始化zookeeper链接.设置回调函数
     * @Author zhanglei
     * @Date 2021/4/19 6:08 下午
     **/
    private void initZookeeper() throws Exception {
        //- init client
        this.zkClient = CuratorFrameworkFactory
                .newClient(serverAddr, new ExponentialBackoffRetry(SLEEP_TIME, RETRY_TIMES));


        ConnectionStateListener listener = new ConnectionStateListener() {
            @Override
            public void stateChanged(CuratorFramework client, ConnectionState newState) {
                if (ConnectionState.CONNECTED == newState) {
                    //- 初始化缓存
                    loadConfig();
                    //- 上报hosts
                    regHosts();
                }
                if (ConnectionState.RECONNECTED == newState){
                    //- 上报hosts
                    regHosts();
                    logger.info("zk 重连成功....");
                }
            }
        };
        zkClient.getConnectionStateListenable().addListener(listener);
        zkClient.start();

        //- first ping to check connect
        zkClient.checkExists().forPath(this.path);
    }




    public void loadConfig(){
        try{
            Stat stat = zkClient.checkExists().forPath(path);
            if (null == stat){
                zkClient.create().creatingParentsIfNeeded().forPath(path);
            }

            //- 监听资源目录
            String rulePath = ZookeeperPathBuilder.buildResourcePath(path);
            if (null == zkClient.checkExists().forPath(rulePath)){
                zkClient.create().creatingParentsIfNeeded().forPath(rulePath);
            }
            addParentListener(rulePath);
        }catch(Throwable e){
            e.printStackTrace();
            logger.error("初始化Another规则失败,exp:{}",e.getMessage(),e);
        }
    }

    @Override
    public void close() {

    }



    private String getResourceNameFromPath(String fullPath){
        return fullPath.substring(fullPath.lastIndexOf("/")+1, fullPath.length());
    }

    /***
     * @Description 添加资源父目录监听
     * @Author zhanglei
     * @Date 2021/4/14 3:11 下午
     **/
    private void addParentListener(String listenPath) throws Exception {
        PathChildrenCache nodeCache = new PathChildrenCache(this.zkClient, listenPath,true);
        PathChildrenCacheListener listener = new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                ChildData childData = pathChildrenCacheEvent.getData();
                if (null == childData){
                    logger.error("检测到zookeeper已宕机，another降级为离线模式...");
                    return;
                }


                String resourceName = getResourceNameFromPath(childData.getPath());
                if (pathChildrenCacheEvent.getType().equals(PathChildrenCacheEvent.Type.CHILD_REMOVED)){
                    refresh(resourceName,null);
                    logger.info("删除Another资源规则:{}",resourceName);
                }else if (pathChildrenCacheEvent.getType().equals(PathChildrenCacheEvent.Type.CHILD_ADDED)
                        || pathChildrenCacheEvent.getType().equals(PathChildrenCacheEvent.Type.CHILD_UPDATED)){
                    byte[] newValue = childData.getData();
                    refresh(resourceName,newValue);
                    logger.info("增加或更新Another资源规则:{}",resourceName);
                }

                //- 异步同步本地磁盘
                syncPool.execute(syncTask);
            }
        };
        nodeCache.getListenable().addListener(listener, this.syncPool);
        nodeCache.start();
    }







    /****
     * @Description  listener变动，刷新本地缓存.
     * @Author zhanglei
     * @Date 2021/3/11 5:05 下午
     **/
    private void refresh(String resourceName,byte[] bytes){
        //- 资源删除操作
        if (bytes == null){
            ruleMapCache.remove(resourceName);
            return;
        }

        //- load path to cache
        String source = new String(bytes);
        final Map<String,String> stringRuleMap = JSON.parseObject(source, new TypeReference<Map<String,String>>(){});
        Map<String,Rule> resourceRuleMap = new HashMap<>();
        for (String key : stringRuleMap.keySet()){
            Rule rule = (Rule) JSON.parseObject(stringRuleMap.get(key), RuleManager.getClassByKey(key));
            rule.init();
            resourceRuleMap.put(key,rule);
        }
        ruleMapCache.put(resourceName,resourceRuleMap);
    }

    
    @Override
    public Map<String, Rule> getRulesByAnotherSource(String sourceName) {
        return ruleMapCache.get(sourceName);
    }
}
