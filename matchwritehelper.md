## Match Condition填写帮助

---

Match Condition是用json来描述入参匹配的方法.如果原方法入参与该json描述一致，则该条规则通过，执行新方法。如不匹配，执行老方法.



### 简单类型入参案例

**说明：包含基本类型、包装类型和字符串类型**



- Java端用 AnotherResource标识方法，AnotherParameterMatch 标识入参，MatchCondition中必须与之一一对应，如下，queryOrder方法入参两个简单类型.

```java
//-原方法
@AnotherResource(name="submitorder",anotherMethod="queryOrderAnother")
public void queryOrder(@AnotherParameterMatch("username") String username,
                        @AnotherParameterMatch("mobile") Long mobile){
    System.out.println("执行老方法....");
}


//- 新方法
public void queryOrderAnother(String username,Long mobile){
    System.out.println("通过another执行新方法....");
}
```



- MatchCondition JSON
  - mobile，username案例解读： 如果(mobile==123456  && username==nero) 则入参匹配通过。否则不通过

```JSON
{
    "mobile": [
        {
            "fieldName": "mobile",
            "matchStrategy": "EQ",
            "parameterAnnotaionName": "mobile",
            "value": "123456"
        }
    ],
    "username": [
        {
            "fieldName": "username",
            "matchStrategy": "EQ",
            "parameterAnnotaionName": "username",
            "value": "nero"
        }
    ]
}
```

- JSON第一层与AnotherParameterMatch的value相对应，如mobile和username
  - fieldName：在复杂类型中用到，意指引用类型的属性.因为是简单类型入参，所以fieldName与外层的mobile相等即可
  - matchStrategy: 匹配策略是约定方法，提供4种
    - EQ : 相等策略，如配置值与入参相等，则匹配，否则不匹配
    - CONTAIN：包含策略，逗号分隔多个，如果入参为配置值的子集，则匹配，否则不匹配
    - LARGERTHAN：配置值大于入参，则匹配，否则不匹配
    - LESSTHAN : 配置值小于入参，则匹配，否则不匹配
  - parameterAnnotaionName： 必填，与AnotherParameterMatch必须匹配
  - value: 配置值



### 引用类型入参

**说明：主要说明引用类型入参的使用**



- 方法示例

```java
//- 引用类型入参
@AnotherResource(name="queryOrder",anotherMethod="queryOrderAnother")
public void queryOrder(@AnotherParameterMatch("orderMain") OrderMain orderMain){
    System.out.println("执行老方法....");
}

public void queryOrderAnother(OrderMain orderMain){
    System.out.println("通过another执行新方法....");
}
```



- 入参对象示例

由于篇幅关系，我这里省略了setter getter.但是它们对Another是强相关的，你必须生成它们

```
public class OrderMain extends OrderMainParent{
    private Long orderId;
    private String orgCode;
    private Integer stationId;
    // setter getter 省略...
}
```



```
public class OrderMainParent {
    private String area;
    // setter getter 省略
}
```



- MatchCondition JSON
  - ordermain案例解读： 如果(orgCode包含1105368 && 124>stationId  &&122 < orderId && area == beijing) .则入参匹配通过,执行新方法，否则不通过执行老方法

```JSON
{
    "orderMain": [
        {
            "fieldName": "orgCode",
            "matchStrategy": "CONTAIN",
            "parameterAnnotaionName": "orderMain",
            "value": "1105368"
        },
        {
            "fieldName": "stationId",
            "matchStrategy": "LARGERTHAN",
            "parameterAnnotaionName": "orderMain",
            "value": 124
        },
        {
            "fieldName": "orderId",
            "matchStrategy": "LESSTHAN",
            "parameterAnnotaionName": "orderMain",
            "value": 122
        },
        {
            "fieldName": "area",
            "matchStrategy": "EQ",
            "parameterAnnotaionName": "orderMain",
            "value": "beijing"
        }
    ]
}
```

- JSON第一层与AnotherParameterMatch的value相对应，如ordermain
  - fieldName:意指引用类型的属性，如上文中的orgCode、orderId等，可以多个，可指定父类属性，如area
  - matchStrategy: 匹配策略是约定方法，提供4种
    - EQ : 相等策略，如配置值与入参相等，则匹配，否则不匹配
    - CONTAIN：包含策略，逗号分隔多个，如果入参为配置值的子集，则匹配，否则不匹配
    - LARGERTHAN：配置值大于入参，则匹配，否则不匹配
    - LESSTHAN : 配置值小于入参，则匹配，否则不匹配
  - parameterAnnotaionName： 必填，与AnotherParameterMatch必须匹配
  - value: 配置值