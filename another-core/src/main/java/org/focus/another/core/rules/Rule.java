package org.focus.another.core.rules;

public interface Rule {


    /****
     * @Description 抽象规格的检查机制
     * @Author zhanglei
     * @Date 2021/3/9 11:30 上午
     **/
    boolean check(Object... objects);


    /***
     * @Description  设定init方法是因为dashboard的写入不会关心业务逻辑，比如frequencyRule中的currentLimit和时间初始化等
     * @Author zhanglei
     * @Date 2021/4/14 10:48 上午
     **/
    void init();



}
