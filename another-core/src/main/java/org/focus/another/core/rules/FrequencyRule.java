package org.focus.another.core.rules;

/****
 * @Description  频率规则
 * @Author zhanglei
 * @Date 2021/3/8 3:11 下午
 **/
public class FrequencyRule extends AbstractRule{
    //- 初始化限制执行次数
    private Integer initLimit;
    //- 每一次限制的时间窗口，单位为秒
    private Integer timeWindowSec;
    //- 下一次限制步长
    private Integer limitStep;


    //- 当前剩余限定次数
    private volatile int currentLimit;
    //- 限频迭代次数
    private Integer stepCount;
    //- 最后一次的重置时间
    private Long lastSetTime;


    //- 区分两种模式， true窗口模式, false递增模式
    private Boolean frequencyMode;


    public boolean isFrequencyMode() {
        return frequencyMode;
    }

    public void setFrequencyMode(boolean frequencyMode) {
        this.frequencyMode = frequencyMode;
    }

    public int getInitLimit() {
        return initLimit;
    }

    public void setInitLimit(int initLimit) {
        this.initLimit = initLimit;
    }

    public int getTimeWindowSec() {
        return timeWindowSec;
    }

    public void setTimeWindowSec(int timeWindowSec) {
        this.timeWindowSec = timeWindowSec;
    }

    public int getLimitStep() {
        return limitStep;
    }

    public void setLimitStep(int limitStep) {
        this.limitStep = limitStep;
    }

    public int getCurrentLimit() {
        return currentLimit;
    }

    public void setCurrentLimit(int currentLimit) {
        this.currentLimit = currentLimit;
    }

    public int getStepCount() {
        return stepCount;
    }

    public void setStepCount(int stepCount) {
        this.stepCount = stepCount;
    }

    public Long getLastSetTime() {
        return lastSetTime;
    }

    public void setLastSetTime(Long lastSetTime) {
        this.lastSetTime = lastSetTime;
    }

    /****
     * @Description
     * @Author zhanglei
     * @Date 2021/3/9 11:34 上午
     **/
    @Override
    public boolean check(Object... objects) {
        synchronized (this){
            //- 到时间则重置该剩余次数
            if ((System.currentTimeMillis() - lastSetTime) > (timeWindowSec * 1000)){
                this.stepCount++;
                //- 窗口模式
                if (frequencyMode){
                    this.currentLimit = limitStep;
                }else{
                    this.currentLimit = (limitStep * stepCount);
                }
                this.lastSetTime = System.currentTimeMillis();
            }


            //- 当次执行
            if (currentLimit <= 0){
                return false;
            }
            this.currentLimit = currentLimit - 1;
            return true;
        }
    }

    @Override
    public void init() {
        this.currentLimit = initLimit;
        this.stepCount = 0;
        this.lastSetTime = System.currentTimeMillis();
    }


}
