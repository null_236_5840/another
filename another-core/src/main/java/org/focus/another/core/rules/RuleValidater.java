package org.focus.another.core.rules;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.Map;

public class RuleValidater {


    /****
     * @Description  验证matchcondition格式是否正确
     * @Author zhanglei
     * @Date 2021/4/17 3:23 下午
     **/
    public static boolean validate(String source){
        try{
            final Map<String,String> stringRuleMap = JSON.parseObject(source, new TypeReference<Map<String,String>>(){});
            for (String key : stringRuleMap.keySet()){
                Rule rule = (Rule) JSON.parseObject(stringRuleMap.get(key), RuleManager.getClassByKey(key));
            }
        }catch(Exception e){
            return false;
        }
        return true;
    }



}
