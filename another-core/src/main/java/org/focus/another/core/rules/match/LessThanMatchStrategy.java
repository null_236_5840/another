package org.focus.another.core.rules.match;

public class LessThanMatchStrategy implements MatchStrategy{
    private static LessThanMatchStrategy instance = new LessThanMatchStrategy();

    private LessThanMatchStrategy() {
    }

    public static LessThanMatchStrategy getInstance(){
        return instance;
    }



    @Override
    public boolean match(Object configValue, Object inputValue) {
        Long configValueLong,inputValueLong;
        try{
            configValueLong = Long.valueOf(String.valueOf(configValue));
            inputValueLong = Long.valueOf(String.valueOf(inputValue));
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        return configValueLong < inputValueLong;
    }
}
