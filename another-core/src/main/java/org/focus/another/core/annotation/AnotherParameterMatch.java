package org.focus.another.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;



/***
 * @Description 标注在参数上的注解，参数必须是一个AnotherCallback的实现
 * @Author zhanglei
 * @Date 2021/3/9 5:21 下午
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface AnotherParameterMatch {
    String value() default "";
}
