package org.focus.another.core.rules;

import org.focus.another.core.annotation.AnotherParameterMatch;
import org.focus.another.core.rules.match.MatchManager;

import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;

/****
 * @Description 匹配规则，匹配限定的Condition
 * @Author zhanglei
 * @Date 2021/3/8 3:03 下午
 **/
public class MatchRule extends AbstractRule{
    //- 总规则.
    private Map<String,List<Condition>> mapConditions;
    public Map<String, List<Condition>> getMapConditions() {
        return mapConditions;
    }
    public void setMapConditions(Map<String, List<Condition>> mapConditions) {
        this.mapConditions = mapConditions;
    }

    public List<Condition> findCondition(String parameterAnnotationName){
        return mapConditions.get(parameterAnnotationName);
    }



    public static class Condition{
        private String parameterAnnotaionName;
        private String fieldName;
        private String matchStrategy;
        private Object value;

        public String getParameterAnnotaionName() {
            return parameterAnnotaionName;
        }
        public void setParameterAnnotaionName(String parameterAnnotaionName) {
            this.parameterAnnotaionName = parameterAnnotaionName;
        }
        public String getFieldName() {
            return fieldName;
        }
        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }
        public String getMatchStrategy() {
            return matchStrategy;
        }

        public void setMatchStrategy(String matchStrategy) {
            this.matchStrategy = matchStrategy;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }


    /***
     * @Description
     * @Author zhanglei
     * @Date 2021/3/9 11:36 上午
     **/
    @Override
    public boolean check(Object... objects) {
        //- 用户输入的参数
        Object[] parameterObjects = (Object[]) objects[0];
        //- 反射方法解析的参数结构.
        Parameter[] parameters = (Parameter[]) objects[1];


        try{
            for (int i = 0 ; i < parameters.length ; i++){
                Parameter parameter = parameters[i];

                Object currentInput = parameterObjects[i];
                AnotherParameterMatch anotherParameterMatch = parameter.getDeclaredAnnotation(AnotherParameterMatch.class);
                if (null == anotherParameterMatch || "".equals(anotherParameterMatch.value())){
                    continue;
                }

                String parameterAnnotationName = anotherParameterMatch.value();
                List<Condition> conditions = this.findCondition(parameterAnnotationName);
                if (null == conditions || conditions.size() == 0){
                    continue;
                }
                return MatchManager.match(conditions,currentInput,parameter);
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @Override
    public void init() {

    }

}
