package org.focus.another.core.rules;

import org.focus.another.core.utils.HostUtil;

/***
 * @Description  限制beta主机，如为空，则不限制
 * @Author zhanglei
 * @Date 2021/3/8 3:10 下午
 **/
public class HostRule extends AbstractRule{
    private String ips;
    public String getIps() {
        return ips;
    }
    public void setIps(String ips) {
        this.ips = ips;
    }


    /****
     * @Description
     * @Author zhanglei
     * @Date 2021/3/9 11:35 上午
     **/
    @Override
    public boolean check(Object... objects) {
        String hostAddress = HostUtil.getLocalHost();
        if (null == hostAddress || "".equals(hostAddress)){
            return false;
        }

        if (ips.contains(hostAddress)){
            return true;
        }
        return false;
    }

    @Override
    public void init() {

    }
}
