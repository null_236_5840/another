package org.focus.another.core.rules;

/***
 * @Description 开关规则
 * @Author zhanglei
 * @Date 2021/3/8 3:03 下午
 **/
public class SwitchRule extends AbstractRule{
    //- true:open false:close
    private Boolean openOrNot = false;

    public Boolean getOpenOrNot() {
        return openOrNot;
    }

    public void setOpenOrNot(Boolean openOrNot) {
        this.openOrNot = openOrNot;
    }


    @Override
    public boolean check(Object... objects) {
        return openOrNot;
    }

    @Override
    public void init() {

    }
}
