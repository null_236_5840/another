package org.focus.another.core.concurrent;

import java.util.concurrent.ThreadFactory;

public class NamedThreadFactory implements ThreadFactory {

    private String threadName;

    public NamedThreadFactory(String threadName) {
        this.threadName = threadName;
    }

    public Thread newThread(Runnable r) {
        return new Thread(r,threadName);
    }
}
