package org.focus.another.core.rules.match;

public interface MatchStrategy {


    /***
     * @Description 执行策略匹配运算
     * @Author zhanglei
     * @Date 2021/3/15 3:01 下午
     * @param configValue : dashboard配置值
     * @param inputValue :  程序输入值
     * @return : boolean true，匹配成功， false匹配失败
     **/
    boolean match(Object configValue,Object inputValue);

}
