package org.focus.another.core.rules.match;

public class EqMatchStrategy implements MatchStrategy{
    private static EqMatchStrategy instance = new EqMatchStrategy();

    private EqMatchStrategy() {

    }

    public static EqMatchStrategy getInstance(){
        return instance;
    }

    @Override
    public boolean match(Object configValue, Object inputValue) {
        String configValueStr,inputValueStr;
        try{
            configValueStr = String.valueOf(configValue);
            inputValueStr = String.valueOf(inputValue);
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        return configValueStr.equals(inputValueStr);
    }
}
