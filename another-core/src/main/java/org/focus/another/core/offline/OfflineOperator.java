package org.focus.another.core.offline;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.focus.another.core.rules.Rule;
import org.focus.another.core.rules.RuleManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;


/****
 * @Description  离线缓存操作
 * @Author zhanglei
 * @Date 2021/4/20 11:41 上午
 **/
public class OfflineOperator {
    private static final Logger logger = LoggerFactory.getLogger(OfflineOperator.class);
    private static final String DEFAULT_OUTPUT_PATH = "/another/nativerule.json";
    private String outputPath;
    private File outputFile;

    public OfflineOperator(String outputPath) {
        if (null == outputPath){
            outputPath = DEFAULT_OUTPUT_PATH;
        }
        this.outputPath = outputPath;
        this.outputFile = new File(outputPath);
    }


    /****
     * @Description  读取本地缓存方案.
     * @Author zhanglei
     * @Date 2021/4/20 11:38 上午
     **/
    public String load() {
        String res = "";
        try{
            if (!outputFile.exists()){
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new FileReader(outputFile));


            String line = null;
            while((line = bufferedReader.readLine())!=null){
                res += line;
            }


            logger.info("离线模式，从本地缓存加载another规则.....");
            return res;
        }catch(Exception e){
            logger.error("LOAD本地缓存失败..",e);
        }
        return res;
    }






    public void loadToRuleMap(Map<String, Map<String, Rule>> resultMap){
        String resourceRuleStr = load();
        if (null == resourceRuleStr || "".equals(resourceRuleStr)){
            return;
        }

        Map<String, Map<String, JSONObject>> tempMap = (Map<String, Map<String, JSONObject>>) JSON.parse(resourceRuleStr);
        for (String resourcekey : tempMap.keySet()){
            final Map<String, JSONObject> stringRuleMap = tempMap.get(resourcekey);
            Map<String,Rule> resourceRuleMap = new HashMap<>();
            for (String key : stringRuleMap.keySet()){
                Rule rule = (Rule) JSON.parseObject(JSON.toJSONString(stringRuleMap.get(key)), RuleManager.getClassByKey(key));
                rule.init();
                resourceRuleMap.put(key,rule);
            }
            resultMap.put(resourcekey,resourceRuleMap);
        }
    }














    /****
     * @Description  将规则缓存到本地.
     * @Author zhanglei
     * @Date 2021/4/20 11:22 上午
     **/
    public void writeDisk(String writeCache) {
        try{
            File parentFile = outputFile.getParentFile();
            if (!parentFile.exists()){
                parentFile.mkdirs();
            }
            if (!outputFile.exists()){
                outputFile.createNewFile();
            }


            BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
            out.write(writeCache);
            out.close();

            logger.info("刷新本地缓存成功.....");
        }catch(Exception e){
            logger.error("规则缓存到本地失败..",e);
        }
    }


    public static void main(String[] args) throws IOException {
        OfflineOperator offlineOperator = new OfflineOperator("/Users/zhanglei10/another/nativerule.json");
       // offlineOperator.writeDisk("123");
        Map<String, Map<String, Rule>> resultMap = new HashMap<>();
        offlineOperator.loadToRuleMap(resultMap);
    }



}
