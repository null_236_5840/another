package org.focus.another.core.rules;

import java.util.HashMap;
import java.util.Map;

public class RuleManager {
    private static final String SWITCH_RULE_KEY = "switchRule";
    private static final String MATCH_RULE_KEY = "matchRule";
    private static final String HOST_RULE_KEY = "hostRule";
    private static final String FREQUENCY_RULE_KEY = "frequencyRule";

    private static final Map<String,Class> convertMap = new HashMap<String,Class>();
    static{
        convertMap.put(SWITCH_RULE_KEY,SwitchRule.class);
        convertMap.put(MATCH_RULE_KEY,MatchRule.class);
        convertMap.put(HOST_RULE_KEY,HostRule.class);
        convertMap.put(FREQUENCY_RULE_KEY,FrequencyRule.class);
    }



    /***
     * @Description 根据convertMap key获取对应的class映射，以完成从字符串到实体bean的映射转换
     * @Author zhanglei
     * @Date 2021/3/9 11:23 上午
     * @param key :
     * @return : java.lang.Class
     **/
    public static Class getClassByKey(String key){
        return convertMap.get(key);
    }




}
