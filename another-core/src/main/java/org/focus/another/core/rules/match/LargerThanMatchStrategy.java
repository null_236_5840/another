package org.focus.another.core.rules.match;

public class LargerThanMatchStrategy implements MatchStrategy{
    private static LargerThanMatchStrategy instance = new LargerThanMatchStrategy();

    private LargerThanMatchStrategy() {
    }

    public static LargerThanMatchStrategy getInstance(){
        return instance;
    }



    @Override
    public boolean match(Object configValue, Object inputValue) {
        Long configValueLong,inputValueLong;
        try{
            configValueLong = Long.valueOf(String.valueOf(configValue));
            inputValueLong = Long.valueOf(String.valueOf(inputValue));
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        return configValueLong > inputValueLong;
    }
}
