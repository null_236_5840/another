package org.focus.another.core.result;



/***
 * @Description 执行结果对比
 * @Author zhanglei
 * @Date 2021/3/9 2:35 下午
 **/
public interface AnotherCallbackResult {

    /****
     * @Description  抽象行为，检查运行结果，
     * @Author zhanglei
     * @Date 2021/3/9 2:37 下午
     * @return : boolean true 表示使用新的实现 false表示检验不通过，采用老的实现
     **/
    boolean checkResult();
}
