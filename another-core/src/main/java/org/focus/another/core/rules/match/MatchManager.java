package org.focus.another.core.rules.match;

import org.focus.another.core.exception.FieldNotFoundException;
import org.focus.another.core.rules.MatchRule;

import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MatchManager {
    public static final String EQ_STRATEGY = "EQ";
    public static final String CONTAIN_STRATEGY = "CONTAIN";
    public static final String LARGERTHAN_STRATEGY = "LARGERTHAN";
    public static final String LESSTHAN_STRATEGY = "LESSTHAN";

    private static final Map<String,MatchStrategy> strategyMap = new ConcurrentHashMap<>();
    static{
        strategyMap.put(EQ_STRATEGY,EqMatchStrategy.getInstance());
        strategyMap.put(CONTAIN_STRATEGY,ContainMatchStrategy.getInstance());
        strategyMap.put(LARGERTHAN_STRATEGY,LargerThanMatchStrategy.getInstance());
        strategyMap.put(LESSTHAN_STRATEGY,LessThanMatchStrategy.getInstance());
    }




    /****
     * @Description 匹配目标方法参数输入是否适配规则。
     * @Author zhanglei
     * @Date 2021/3/15 2:54 下午
     * @param conditions : 参数对应的所有匹配规则。
     * @param inputValue : 方法输入参数
     * @param parameter : 参数对象，反射类型
     * @return : boolean
     **/
    public static final boolean match(List<MatchRule.Condition> conditions, Object inputValue, Parameter parameter) throws NoSuchFieldException, IllegalAccessException {
        boolean isMatch = true;
        for (MatchRule.Condition condition : conditions){
            String fieldName = condition.getFieldName();
            MatchNode matchNode = toLink(fieldName);

            Class clazz = parameter.getType();
            Object inputValueFinal = getInputValue(matchNode,clazz,inputValue);
            Object configValue = condition.getValue();
            isMatch = strategyMap.get(condition.getMatchStrategy()).match(configValue,inputValueFinal);

            if (!isMatch){
                return isMatch;
            }
        }
        return isMatch;
    }


    /***
     * @Description 将匹配规则name 点号分割的string转换为链表结构
     * @Author zhanglei
     * @Date 2021/3/13 2:26 下午
     * @param name :
     * @return : org.focus.another.core.rules.match.MatchNode
     **/
    private static MatchNode toLink(String name){
        String[] propertys = name.split("\\.");
        MatchNode node = new MatchNode();
        MatchNode head = node;
        for (int i = 0 ; i < propertys.length ; i++){
            node.setNodeName(propertys[i]);

            if (i == propertys.length - 1){
                node.setNext(null);
            }else{
                MatchNode next = new MatchNode();
                node.setNext(next);
                node = next;
            }
        }
        return head;
    }




    /****
     * @Description
     * @Author zhanglei
     * @Date 2021/3/15 2:37 下午
     * @param matchNode :  当前的参数属性节点
     * @param clazz :  当前属性的class类型
     * @param currentParam : 当前参数对象
     * @return : java.lang.Object 获取对应属性值
     **/
    private static Object getInputValue(MatchNode matchNode,Class clazz,Object currentParam) throws NoSuchFieldException, IllegalAccessException {
        //- 区分简单类型和复杂类型
        if (isSimple(clazz)){
            return currentParam;
        }else{
            return getInputValueComplex(matchNode,clazz,currentParam);
        }
    }




    /****
     * @Description 获取入参类型对应的属性
     * @Author zhanglei
     * @Date 2021/4/17 10:45 上午
     * @param propertyName : 属性名
     * @param clazz : 字段类型，随着层级变动
     * @param sourceClazz : 字段类型原始类型
     * @return : java.lang.reflect.Field
     **/
    private static Field getField(String propertyName,Class clazz,Class sourceClazz) throws NoSuchFieldException {
        if (null == clazz){
            throw new FieldNotFoundException(String.format("参数类型为:%s,属性为:%s,未找到..",sourceClazz.toString(),propertyName));
        }

        Field field = null;
        try{
            field = clazz.getDeclaredField(propertyName);
        } catch (NoSuchFieldException e) {
            field = getField(propertyName,clazz.getSuperclass(),clazz);
        }
        return field;
    }





    /****
     * @Description 判断入参类型是简单类型还是复杂类型，从而决定要不要getField
     * @Author zhanglei
     * @Date 2021/4/17 11:49 上午
     **/
    private static boolean isSimple(Class clazz){
        try {
            if (String.class.equals(clazz) || ((Class<?>)clazz.getField("TYPE").get(null)).isPrimitive()){
                return true;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (NoSuchFieldException e) {
            //- 非简单类型.

            return false;
        }

        return false;
    }



    /****
     * @Description 复杂类型获取输入值
     * @Author zhanglei
     * @Date 2021/4/17 11:52 上午
     * @param matchNode :
     * @param clazz :
     * @param currentParam :
     * @return : java.lang.Object
     **/
    private static Object getInputValueComplex(MatchNode matchNode,Class clazz,Object currentParam) throws NoSuchFieldException, IllegalAccessException {
        String propertyName = matchNode.getNodeName();
        Field field = getField(propertyName,clazz,clazz);
        field.setAccessible(true);

        while(matchNode.getNext() != null){
            Object nextParam = field.get(currentParam);
            return getInputValue(matchNode.getNext(),field.getType(),nextParam);
        }

        return field.get(currentParam);
    }
}
