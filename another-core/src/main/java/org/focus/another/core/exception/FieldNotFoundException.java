package org.focus.another.core.exception;

public class FieldNotFoundException extends RuntimeException{

    public FieldNotFoundException(String message) {
        super(message);
    }
}
