package org.focus.another.core.rules.match;

/***
 * @Description  参数匹配层级，用链表表示
 * @Author zhanglei
 * @Date 2021/3/13 1:59 下午
 **/
public class MatchNode {
    private String nodeName;
    private MatchNode next;

    public MatchNode getNext() {
        return next;
    }

    public void setNext(MatchNode next) {
        this.next = next;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }
}
