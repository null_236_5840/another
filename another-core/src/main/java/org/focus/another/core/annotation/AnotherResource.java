package org.focus.another.core.annotation;

import java.lang.annotation.*;

/****
 * @Description 试用版资源注解，标注试用资源
 * @Author zhanglei
 * @Date 2021/3/8 3:30 下午
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface AnotherResource {

    //- 方法的唯一资源，默认值为应用级模板
    String name() default "another.app.common";

    //- 指定b方法名，作为灰度方法
    String anotherMethod() default "";
}
