package org.focus.another.core.rules.match;


public class ContainMatchStrategy implements MatchStrategy{
    private static ContainMatchStrategy instance = new ContainMatchStrategy();
    private ContainMatchStrategy() {

    }
    public static ContainMatchStrategy getInstance(){
        return instance;
    }

    @Override
    public boolean match(Object configValue, Object inputValue) {
        String configValueStr,inputValueStr;
        try{
            configValueStr = String.valueOf(configValue);
            inputValueStr = String.valueOf(inputValue);
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        return configValueStr.contains(inputValueStr);
    }
}
